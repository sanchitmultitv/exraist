import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgendaThreeeComponent } from './agenda-threee.component';

describe('AgendaThreeeComponent', () => {
  let component: AgendaThreeeComponent;
  let fixture: ComponentFixture<AgendaThreeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendaThreeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgendaThreeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
