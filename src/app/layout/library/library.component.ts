import { Component, OnInit } from '@angular/core';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit {

  constructor(private toastr:ToastrService,private _fd: FetchDataService) { }

  ngOnInit(): void {
  }
  goDownloadPoints(){
    let data3 = localStorage.getItem('downloaddata');
    if (data3 == '20') {
      
    } else {
      localStorage.setItem('downloaddata', '20');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('downloaddata')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','downloaddata')
      
      this._fd.clickPoint(formData).subscribe((res: any) => {
  
      });
      this.toastr.success( '20 Points Added');

    }
  }
  goPhotoPoints(){
    let data3 = localStorage.getItem('photogallery');
    if (data3 == '10') {
      
    } else {
      localStorage.setItem('photogallery', '10');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('photogallery')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','photogallery')

      this._fd.clickPoint(formData).subscribe((res: any) => {
  
      });
      this.toastr.success( '10 Points Added');
      
    }
  }
}
