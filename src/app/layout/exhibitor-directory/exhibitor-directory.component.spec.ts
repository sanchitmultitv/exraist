import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhibitorDirectoryComponent } from './exhibitor-directory.component';

describe('ExhibitorDirectoryComponent', () => {
  let component: ExhibitorDirectoryComponent;
  let fixture: ComponentFixture<ExhibitorDirectoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExhibitorDirectoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExhibitorDirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
