import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-exhibitor-directory',
  templateUrl: './exhibitor-directory.component.html',
  styleUrls: ['./exhibitor-directory.component.scss']
})
export class ExhibitorDirectoryComponent implements OnInit {

  constructor(private router: Router) {}

  ngOnInit(): void {
  }
  closePopup(){
    $('.exDirectory').modal('hide');
  }
  lifeboy(title){
    this.router.navigate(['/exhibitionHall/lifeboy',title]);
    $('.exDirectory').modal('hide');
    
  }
  lifeboy2(title2){
    this.router.navigate(['/exhibitionHall/',title2]);
    $('.exDirectory').modal('hide');
    
  }
}
