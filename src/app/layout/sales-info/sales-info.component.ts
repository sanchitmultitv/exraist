import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import  * as html2canvas  from 'html2canvas';
import {ToastrService} from 'ngx-toastr';
declare var $:any;
@Component({
  selector: 'app-sales-info',
  templateUrl: './sales-info.component.html',
  styleUrls: ['./sales-info.component.scss']
})
export class SalesInfoComponent implements OnInit {
salesData;
element;
getCanvas;
@ViewChild('screen') screen: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('downloadLink') downloadLink: ElementRef;
  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
    this.salesData = JSON.parse(localStorage.getItem('exhibitData'));
  }
  closePopup(){
    $('.salesModal').modal('hide');
  }
  download(){
    html2canvas(this.screen.nativeElement).then(canvas => {
      this.canvas.nativeElement.src = canvas.toDataURL();
      this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
      this.downloadLink.nativeElement.download = 'karan.png';
      this.downloadLink.nativeElement.click();
      
    });
    this.toastr.success( 'Card Dropped Succesfully!');
  }
}
