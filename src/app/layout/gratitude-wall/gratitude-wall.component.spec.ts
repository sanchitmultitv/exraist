import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GratitudeWallComponent } from './gratitude-wall.component';

describe('GratitudeWallComponent', () => {
  let component: GratitudeWallComponent;
  let fixture: ComponentFixture<GratitudeWallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GratitudeWallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GratitudeWallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
