import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { KbcComponent } from './kbc/kbc.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
import { AudioScreenComponent } from './audio-screen/audio-screen.component';
import { VisitorProfileComponent } from '../core-components/visitor-profile/visitor-profile.component';
import { Exhibition2Component } from '../core-components/exhibition-hall/exhibition2/exhibition2.component';
import { AgendaComponent } from './agenda/agenda.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { AgendaTwoComponent } from './agenda-two/agenda-two.component';
import { AgendaThreeeComponent } from './agenda-threee/agenda-threee.component';
import { LibraryComponent } from './library/library.component';

import { HeightlightComponent } from './heightlight/heightlight.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { PavillionComponent } from './pavillion/pavillion.component';
import { Acma1Component } from './acma1/acma1.component';
import { GratitudeWallComponent } from './gratitude-wall/gratitude-wall.component';
import { MyContactsComponent } from '../core-components/my-contacts/my-contacts.component';
import { VideoGalleryComponent } from './video-gallery/video-gallery.component';
import { DocumentsComponent } from './documents/documents.component';
import { ContributeEraComponent } from './contribute-era/contribute-era.component';
import { PhotoboothComponent } from './photobooth/photobooth.component';

// import { from } from 'core-js/fn/array';



const routes: Routes = [
  { path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      // {path: 'index', loadChildren: ()=> import('../core-components/index/index.module').then(m => m.IndexModule)},
      {path: 'lobby', loadChildren: ()=> import('../core-components/lobby/lobby.module').then(m => m.LobbyModule)},
      {path: 'welcome', loadChildren: ()=> import('../core-components/welcome-lobby/welcome-lobby.module').then(m => m.WelcomeLobbyModule)},
      {path: 'auditorium', loadChildren: ()=> import('../core-components/auditorium/auditorium.module').then(m => m.AuditoriumModule)},
      {path: 'exhibitionHall', loadChildren: ()=> import('../core-components/exhibition-hall/exhibition-hall.module').then(m => m.ExhibitionHallModule)},
      // {path: 'exhibitionTwo', loadChildren: ()=> import('../core-components/exhibition-two/exhibition-two.module').then(m => m.ExhibitionTwoModule)},
      // {path: 'exhibitionThree', loadChildren: ()=> import('../core-components/exhibition-three/exhibition-three.module').then(m => m.ExhibitionThreeModule)},
      {path: 'registrationDesk', loadChildren: ()=> import('../core-components/registration-desk/registration-desk.module').then(m => m.RegistrationDeskModule)},
      {path: 'networkingLounge', loadChildren: ()=> import('../core-components/networking-lounge/networking-lounge.module').then(m => m.NetworkingLoungeModule)},
      {path: 'stage', loadChildren: ()=> import('../core-components/stage/stage.module').then(m => m.StageModule)},
      {path: 'meetingRoom', loadChildren: ()=> import('../core-components/meeting-room/meeting-room.module').then(m => m.MeetingRoomModule)},
      {path: 'chats', loadChildren: ()=> import('../core-components/chat/chat.module').then(m => m.ChatModule)},
      {path: 'myContacts', loadChildren: ()=> import('../core-components/my-contacts/my-contacts.module').then(m => m.MyContactsModule)},
      {path: 'briefcase', loadChildren: ()=> import('../core-components/briefcase/briefcase.module').then(m => m.BriefcaseModule)},
      {path:'quiz', component:KbcComponent},
      {path:'capturePhoto', component:CapturePhotoComponent},
      {path: 'audioScreen', component:AudioScreenComponent},
      {path: 'profiles', component:VisitorProfileComponent},
      {path: 'agenda', component:AgendaComponent},
      {path: 'speakers', component:SpeakersComponent},
      {path: 'contribute', component:PavillionComponent},
      {path: 'contribute-era', component:ContributeEraComponent},
      {path: 'agenda-two', component:AgendaTwoComponent},
      {path: 'agenda-three', component:AgendaThreeeComponent},
      {path: 'library',component:LibraryComponent},
      {path: 'sponsors', component:SponsorsComponent},
      {path: 'stadium', component:HeightlightComponent},
      {path: 'funzone', component:Acma1Component},
      {path: 'gratitude-wall', component:GratitudeWallComponent},
      {path: 'photo-gallery', component:MyContactsComponent},
      {path: 'video-gallery', component:VideoGalleryComponent},
      {path: 'documents', component:DocumentsComponent},
      {path: 'photobooth',component:PhotoboothComponent}
    
      // {path: 'exhibitionHall/exhibition2', component:Exhibition2Component},

    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
