import { Component, OnInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;
@Component({
  selector: 'app-ballroom-four',
  templateUrl: './ballroom-four.component.html',
  styleUrls: ['./ballroom-four.component.scss']
})
export class BallroomFourComponent implements OnInit,OnDestroy {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.getQA();
  //   this.interval = setInterval(() => {
  //    this.getQA();
  //  }, 100000);

 }
 closePopup() {
   $('.ballroomQuestionModalFour').modal('hide');
 }
 getQA(){
   let data = JSON.parse(localStorage.getItem('virtual'));
  //  this._fd.Liveanswers().subscribe((res=>{
  //    console.log(res);
  //    this.qaList = res.result;
  //  }))
 }
 postQuestion(value){
   let data = JSON.parse(localStorage.getItem('virtual'));
   let audi_id = '4';
  console.log(value, data.id, audi_id);
   this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
     //console.log(res);
     if(res.code == 1){
       this.msg = 'Submitted Succesfully';
     }
     this.textMessage.reset();
     $('.ballroomQuestionModalFour').modal('toggle');
   }))
 }
 ngOnDestroy() {
   clearInterval(this.interval);
 }

}
