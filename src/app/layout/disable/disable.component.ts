import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-disable',
  templateUrl: './disable.component.html',
  styleUrls: ['./disable.component.scss']
})
export class DisableComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }
  closePopup() {
    $('.disableModal').modal('hide');
  }
}
