import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $: any;

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.scss']
})
export class SpeakersComponent implements OnInit {

  constructor(private sanitiser: DomSanitizer,private _fd: FetchDataService) { }
  profiles: any ;
  document;
  data:any
  documentName;
  speakerName;
  speakerContent;
  speakerPosition;
  profileObj:any = {};
  ngOnInit(): void {
    this.getImages();
  }
  getDocument() {
    $('.speakersDocument').modal('show');
    // window.open("https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/speakers_profiles/Kenichi Ayukawa/Brief Profile - Mr. Kenichi Ayukawa.pdf","viewer"); 
    // this.document=this.sanitiser.bypassSecurityTrustResourceUrl(prof.file);
    // this.documentName = prof.name;
  }
  getImages(){
    this._fd.getImage().subscribe(res => {
    this.data  = res;
    this.profiles = this.data.result;
    console.log(this.data.result+'help please')
    });
  }


  getDetails(profile) {
    this.profileObj = profile;
    $('.speakersDocument').modal('show');
  }
  closedocumentPopup() {
    $('.speakersDocument').modal('hide');

  }
}
