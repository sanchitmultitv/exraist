import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributeEraComponent } from './contribute-era.component';

describe('ContributeEraComponent', () => {
  let component: ContributeEraComponent;
  let fixture: ComponentFixture<ContributeEraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributeEraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributeEraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
