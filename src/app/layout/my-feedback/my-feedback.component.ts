import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import {FetchDataService} from '../../services/fetch-data.service'

declare var $: any;

@Component({
  selector: 'app-my-feedback',
  templateUrl: './my-feedback.component.html',
  styleUrls: ['./my-feedback.component.scss']
})
export class MyFeedbackComponent implements OnInit {
  ratingForm = new FormGroup({
    
   password: new FormControl('', [Validators.required]),
  });
  ratingList=[];
  ratingActual = [];
  feedbackList=[];
  feedbackFormActualData = [];
   event_id = 123;
   player;
  videoName;
  // babyVideo =[];

  constructor(private _fd: FetchDataService) { }

  ngOnInit(): void {
    
//     this._fd.getRating(this.event_id).subscribe(res=>{
// this.ratingList = res.result;

// console.log('response check', this.ratingList)
//     })
   
     
  

    // this._fd.getFeedback(this.event_id).subscribe(res=>{
    //   this.feedbackList=res.result;
    //  console.log('check respons',this.feedbackList);
      
    // })
    
    let playVideo: any = document.getElementById("playVideoUrl");
    window.onclick = (event) => {
      //alert('akaia')
      if (playVideo.playing) {
        alert('ji');
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime=0;
        pauseVideo.pause();
        this.player='';
      }
    }
  }

  playVideo1() {
    this.player = 'https://exdemo.multitvsolution.com/assets/reinvest/video/baby.mp4';
    this.videoName = 'baby';
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideoUrl').modal('show');
    // $('.feebackModal').modal('hide');
  }
  playVideo2() {
    this.player = 'https://exdemo.multitvsolution.com/assets/reinvest/video/boy.mp4';
    this.videoName = 'boy';
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    // $('.feebackModal').modal('hide');
    $('#playVideoUrl').modal('show');
  }
  playVideo3() {
    this.player = 'https://exdemo.multitvsolution.com/assets/reinvest/video/clinic.mp4';
    this.videoName = 'clinic';
    $('#playVideoUrl').modal('show');
    // $('.feebackModal').modal('hide');
  }
  playVideo4() {
    this.player = 'https://exdemo.multitvsolution.com/assets/reinvest/video/doctors.mp4';
    this.videoName = 'doctors';
    $('#playVideoUrl').modal('show');
    // $('.feebackModal').modal('hide');
  }

  closePopup(){
    $('.feebackModal').modal('hide');
  }

  closevideoPopup() {
    let pauseVideo: any = document.getElementById("video");
    console.log(pauseVideo);
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    this.player = '';
    $('#playVideoUrl').modal('hide');
  }
  radioValue(rateValue, radio){
    let rateingValue = rateValue
    let ratingID = radio
    let rateingData = {'rating_id':ratingID, 'rate':rateingValue}
    this.ratingActual.push(rateingData);

  }


  submitFeedback(){
    let userID: any = JSON.parse(localStorage.getItem('virtual'));
    let agenda = (<HTMLInputElement>document.getElementById("agenda")).value;
   let registration= (<HTMLInputElement>document.getElementById("registration")).value;
   let event= (<HTMLInputElement>document.getElementById("event")).value;


   let formDataNew:any = this.ratingActual

let data:any =[{'feedback_id':1, 'feedback':agenda}, {'feedback_id':2, 'feedback':registration},{'feedback_id':3, 'feedback':event}];

    const formData = new FormData();
    formData.append('rate', JSON.stringify(formDataNew));
    formData.append('user_id', userID.id);
   console.log('rating data', formData)
    this._fd.postRating(formData).subscribe((res: any) => {
      if(res.code===1){
        $('.feebackModal').modal('hide');
      }
    })

    const newFormData = new FormData()
newFormData.append('feedback', JSON.stringify(data));
newFormData.append('user_id', userID.id);

console.log('feedback check', newFormData);

    this._fd.postFeedback(newFormData).subscribe((res: any) => {
      if(res.code===1){
        $('.feebackModal').modal('hide');
      }
    })
    
    

  }
  
}
