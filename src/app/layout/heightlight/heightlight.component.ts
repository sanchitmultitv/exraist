import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-heightlight',
  templateUrl: './heightlight.component.html',
  styleUrls: ['./heightlight.component.scss']
})
export class HeightlightComponent implements OnInit {
  videoSource;
  constructor() { }
  videoPlayerTwo = 'https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session2.smil/playlist.m3u8';

  ngOnInit(): void {
  }
  closePopup(){
    $('.heightlightModal').modal('hide');
  }
  closecreative(val){
    $(val).modal('hide');
  }
  playShowVideo(video) {
    this.videoSource = video;
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }
  
  closeModalVideo(){
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }
}
