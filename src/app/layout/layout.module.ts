import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { ProfileComponent } from './profile/profile.component';
import { AttendeesComponent } from './attendees/attendees.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ChatsComponent } from './chats/chats.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyFeedbackComponent } from './my-feedback/my-feedback.component';
import { KbcComponent } from './kbc/kbc.component';
import { CapturePhotoComponent } from './capture-photo/capture-photo.component';
import { QuestionComponent } from './question/question.component';
import { PollComponent } from './poll/poll.component';
import {WtsppFileComponent} from './wtspp-file/wtspp-file.component';
import { HeightlightComponent } from './heightlight/heightlight.component';
import { AudioScreenComponent } from './audio-screen/audio-screen.component';
import { BallroomQuestionComponent } from './ballroom-question/ballroom-question.component';
import { GroupChatComponent } from './group-chat/group-chat.component';
import { DocsInfoComponent } from './docs-info/docs-info.component';
import { SalesInfoComponent } from './sales-info/sales-info.component';
import { ScheduleCallComponent } from './schedule-call/schedule-call.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BriefcaseDocsComponent } from './briefcase-docs/briefcase-docs.component';
import { PollTwoComponent } from './poll-two/poll-two.component';
import { PollThreeComponent } from './poll-three/poll-three.component';
import { PollFourComponent } from './poll-four/poll-four.component';
import { BallroomTwoComponent } from './ballroom-two/ballroom-two.component';
import { BallroomThreeComponent } from './ballroom-three/ballroom-three.component';
import { BallroomFourComponent } from './ballroom-four/ballroom-four.component';
import { ActiveAudiComponent } from './active-audi/active-audi.component';
import { GroupChatTwoComponent } from './group-chat-two/group-chat-two.component';
import { GroupChatThreeComponent } from './group-chat-three/group-chat-three.component';
import { GroupChatFourComponent } from './group-chat-four/group-chat-four.component';
import { FaqsComponent } from './faqs/faqs.component';
import { VisitorDashboardComponent } from './visitor-dashboard/visitor-dashboard.component';
import { ExhibitorDirectoryComponent } from './exhibitor-directory/exhibitor-directory.component';
import { Acma1Component } from './acma1/acma1.component';
import { Acma2Component } from './acma2/acma2.component';
import { Acma3Component } from './acma3/acma3.component';
import { Acma4Component } from './acma4/acma4.component';
import { SpeakersComponent } from './speakers/speakers.component';
import { AgendaTwoComponent } from './agenda-two/agenda-two.component';
import { AgendaThreeeComponent } from './agenda-threee/agenda-threee.component';
import { LibraryComponent } from './library/library.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgBufferingModule } from 'videogular2/compiled/buffering';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgStreamingModule } from 'videogular2/compiled/streaming';
import { AppointmentsComponent } from './appointments/appointments.component';
import { DisableComponent } from './disable/disable.component';
import { PavillionComponent } from './pavillion/pavillion.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { GratitudeWallComponent } from './gratitude-wall/gratitude-wall.component';
import { VideoGalleryComponent } from './video-gallery/video-gallery.component';
import { DocumentsComponent } from './documents/documents.component';
import { ContributeEraComponent } from './contribute-era/contribute-era.component';
import { PhotoboothComponent } from './photobooth/photobooth.component';





@NgModule({
  declarations: [
    LayoutComponent,
    ProfileComponent,
    AttendeesComponent,
    AgendaComponent,
    ChatsComponent,
    KbcComponent,
    CapturePhotoComponent,
    QuestionComponent,
    PollComponent,
    WtsppFileComponent,
    MyFeedbackComponent, HeightlightComponent, AudioScreenComponent, BallroomQuestionComponent, GroupChatComponent, DocsInfoComponent, SalesInfoComponent, ScheduleCallComponent, BriefcaseDocsComponent, PollTwoComponent, PollThreeComponent, PollFourComponent, BallroomTwoComponent, BallroomThreeComponent, BallroomFourComponent, ActiveAudiComponent, GroupChatTwoComponent, GroupChatThreeComponent, GroupChatFourComponent, FaqsComponent, VisitorDashboardComponent, Acma1Component, Acma2Component, Acma3Component, Acma4Component,ExhibitorDirectoryComponent, SpeakersComponent,AgendaTwoComponent, AgendaThreeeComponent, LibraryComponent, AppointmentsComponent, DisableComponent, PavillionComponent, SponsorsComponent, GratitudeWallComponent, VideoGalleryComponent, DocumentsComponent, ContributeEraComponent, PhotoboothComponent],
exports:[MyFeedbackComponent, GroupChatComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutRoutingModule,
    NgbModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule

  ]
})
export class LayoutModule { }
