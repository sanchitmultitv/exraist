import { Injectable } from '@angular/core';
import { Login } from './module/login';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ApiConstants } from './apiConfig/api.constants';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {
  baseUrl = environment.baseUrl;
  loginBaseUrl = environment.url;
  login = 'RecognizerApi/index.php/api/engine/authentication';


  constructor(private http: HttpClient) { }
  authLogin(user: Login): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('email', user.email);
    params = params.set('password', user.password);
    return this.http.post(`${this.loginBaseUrl}/${this.login}`, params);
  }

  getProfile(id, role_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.profile}/id/${id}/role_id/${role_id}`);
  }
  getAttendees(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}`);
  }
  getAttendeesbyName(event_id,name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.attendees}/${event_id}/name/${name}`);
  }
  getComments(event_id, user_id, typ): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.commentsList}/event_id/${event_id}/type/${typ}`);
  }
  postComments(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.addComment}`, comment);
  }
  getOne2oneChatList(event_id, name): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatAttendeesComments}/event_id/${event_id}/name/${name}`);
  }
  enterTochatList(receiver_id, sender_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.One2OneCommentList}/receiver_id/${receiver_id}/sender_id/${sender_id}`)
  }
  postOne2oneChat(comment: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.one2oneCommentPost}`, comment);
  }
  // getRating(event_id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.ratingMaster}/event_id/${event_id}`);
  // }
  // getFeedback(event_id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.feedbackMaster}/event_id/${event_id}`);
  // }
  postFeedback(feedback: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/feedback/post`, feedback);
  }
  postRating(rating: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/virtualapi/v1/rating/post`, rating);
  }
  getExhibition(title,cat):Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibition}/category/${cat}/id/${title}`);
  }
  getExhibitionTwo(id):Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.exhibitiontwo}/${id}`);
  }
  
  getQuiz() {
    return this.http.get('https://opentdb.com/api.php?amount=50&category=11&type=multiple');
  }

  askQuestions(id,name, value,exhibit_id, exhibit_name): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('sender_id', id);
    params = params.set('sender_name', name);
    params = params.set('receiver_id', exhibit_id);
    params = params.set('receiver_name', exhibit_name);
    params = params.set('msg', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestion}`, params);
  }
  helpdesk(id,value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('event_id', '57');
    params = params.set('user_id', id);
    params = params.set('question', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.helpdesk}`, params);
  }
  askLiveQuestions(id, value, audi_id): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('user_id', id);
    params = params.set('question', value);
    params = params.set('event_id', '57');
    params = params.set('audi_id', audi_id);

    return this.http.post(`${this.baseUrl}/${ApiConstants.askQuestionLive}`, params);
  }
  getanswers(uid,exhibit_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getQuestionAnswser}/${uid}/sender_id/${exhibit_id}`)
  }
  gethelpdeskanswers(uid): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.helpdeskAnswes}/${uid}/event_id/57`)
  }
  getCard(exhibition_id){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/card/drop/list/exhibition_id/${exhibition_id}`)
  }
  getstandarad(){
    return this.http.get(`${this.baseUrl}/${ApiConstants.standard}`)
  }
  getpremium(){
    return this.http.get(`${this.baseUrl}/${ApiConstants.premium}`)
  }
  // getstate(){
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.state}`)
  // }
  // getcountry(){
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.country}`)
  // }
  // Liveanswers(): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getliveAnswser}`)
  // }
  getPollList(id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  }
  getChatData(id):Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.chatHistory}/${id}`)
  }
  // getPollListTwo(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListThree(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  // getPollListFour(id): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.getPolls}/${id}`);
  // }
  postPoll(id, data, value): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('poll_id', id);
    params = params.set('user_id', data);
    params = params.set('answer', value);
    return this.http.post(`${this.baseUrl}/${ApiConstants.postPolls}`, params);
  }
  getWtsappFiles(): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.wtsappAgendaFiles}`);
  }params

  getQuizList(event_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.quizlist}/event_id/${event_id}`);
  }
  postSubmitQuiz(quiz): Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.submitQuiz}`, quiz);
  }
  getSummaryQuiz(event_id, user_id): Observable<any> {
    return this.http.get(`${this.baseUrl}/${ApiConstants.summaryQuiz}/event_id/${event_id}/user_id/${user_id}`);
  }

  uploadCameraPic(pic): Observable<any> {
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/upload/pic`, pic);
  }
  uploadsample(pic): Observable<any>{
    // return this.http.post(`https://tbbmedialive.com/Q3PCM2020/uploadblob.php`, pic);
    return this.http.post(`https://virtualapi.multitvsolution.com/upload_photo/uploadblob.php`, pic);
  }
  groupchating(): Observable<any>{
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/mongo/groupchat/room/exraist`);
  }

  groupchatingtwo(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_2`);
  }
  groupchatingthree(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_3`);
  }
  groupchatingfour(): Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.groupChats}/serdia_4`);
  }
  submitWheelScore(wheel:any){
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/user/game/post`, wheel);
  }
  getBriefcaseList(event_id, user_id){
    return this.http.get(`${this.baseUrl}/${ApiConstants.briefcaseList}/event_id/${event_id}/user_id/${user_id}`);
  }
  postBriefcase(doc:any){
    return this.http.post(`${this.baseUrl}:7000/${ApiConstants.postBriefcase}`, doc);
  }
  postGroup(group:any){
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/groupchat/post`,group);
  }
  postDoc(users): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    // let params = new HttpParams();
    // params = params.set('exhibition_id', users.exhibition_id);
    // params = params.set('title', users.titles);
    // params = params.set('doc', users.files);
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/upload/doc`, users);
  }
  postBrochure(use): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/upload/brochure`, use);
  }

  postCard(data:any){
    return this.http.post(`${this.baseUrl}/${ApiConstants.postCardDrop}`, data);
  }
  analyticsPost(analytics: any): Observable<any>{
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/analytics/user/history/add`, analytics);
  }
  // activeAudi():Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.activeInactive}`);
  // }
  schdeuleAcall(dates):Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.scheduleaCall}`,dates);
  }
  schdeuleAcallattende(dates):Observable<any> {
    return this.http.post(`${this.baseUrl}/${ApiConstants.attendeSchedulecall}`,dates);
  }
  scheduleCallGet(locid):Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/user/schedule/call/list/exhibition_id/${locid}`);
  }
  getScheduleList(uid):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.callListScheduled}/${uid}`);
  }
  getScheduleListatttendee(uid):Observable<any>{
    return this.http.get(`${this.baseUrl}/${ApiConstants.callschdeuledAttendee}/${uid}`);
  }
  getChatPerson(guy){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/asked/question/list/exhibition_id/${guy}`);
  }

  getParticularChat(id, guy2){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/asked/question/answer/get/user_id/${guy2}/event_id/54/exhibition_id/${id}/flag/desc`)
  }
  // delDoc(exhibi_id, id):Observable<any> {
  //   const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
  //   let params = new HttpParams();
  //   params = params.set('exhibition_id', exhibi_id);
  //   params = params.set('id', id);
  //   return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/delete/exhibition/doc`,params)
  // }

  postReply(data): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    // let params = new HttpParams();
    // params = params.set('exhibition_id', users.exhibition_id);
    // params = params.set('title', users.titles);
    // params = params.set('doc', users.files);
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/reply/question/post`,data);
  }
  getRed(): Observable<any> {
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/asked/question/not_replied/exhibition_id/27`);
  }
  getAnalytics(value){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/analytics/user/history/event_id/54/action/${value}`);
  }
  // getBrouchers(ex_id, uid): Observable<any> {
  //   return this.http.get(`${this.baseUrl}/${ApiConstants.brouchers}/${ex_id}/user_id/${uid}`)
  // }
  totalTimeSlots(ex_id,date):Observable<any> {
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/calender.php?exhibition_id=${ex_id}&date=${date}`) 
  }
  delCall(cancel): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    let params = new HttpParams();
    params = params.set('id', cancel.idd);
    params = params.set('email', cancel.emaill);
    params = params.set('time', cancel.timee);
    params = params.set('ec_name', cancel.exhii);

    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/cancel/schedule/call`,params);
  }
  downXL(xl){
    return this.http.get(`https://virtualapi.multitvsolution.com/upload_photo/report.php?event_id=54&action=${xl}`);
  }

  getReinvestAttendeesCoreSector(event_id, core_sector){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/country/india`);
  }
  getReinvestAttendeesCoreBusiness(event_id, core_business){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/${event_id}/core_business/${core_business}/country/india`);
  }
  getReinvestAttendees(event_id, core_sector, core_business){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/core_business/${core_business}/country/india`);
  }
  ReinvestAttendeesCoreSectorSearch(event_id, core_sector, name){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/country/india/name/${name}`);
  }
  ReinvestAttendeesCoreBusinessSearch(event_id, core_business, name){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/${event_id}/core_business/${core_business}/country/india/name/${name}`);
  }
  ReinvestAttendees(event_id, core_sector, core_business, name){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/attendees/event_id/${event_id}/core_sector/${core_sector}/core_business/${core_business}/country/india/name/${name}`);
  }
  usermanagement(status:any):Observable<any>{
    return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/attendee/online/update`, status);
  }
  ExhiSearch(){
    return this.http.get(`https://vodstorage-streamline.s3.ap-south-1.amazonaws.com/exhibition.json`);
  }
  getImage(){
    return this.http.get(`https://virtualapi.multitvsolution.com/exhibition.php`);
  }
  // getVideo(){
  //   return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/gallery/event_id/61/type/video`);
  // }
  
  clickPoint(data): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
      return this.http.post(`https://goapi.multitvsolution.com:7000/virtualapi/v1/user/points/create/event_id/61`,data);
  }
  getClickData(){
    return this.http.get(`https://goapi.multitvsolution.com:7000/virtualapi/v1/get/points/event_id/61`);
  }
}
