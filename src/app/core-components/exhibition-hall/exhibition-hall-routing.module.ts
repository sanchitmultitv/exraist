import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibitionHallComponent } from './exhibition-hall.component';
import { ExhibitLifeComponent } from './exhibit-life/exhibit-life.component';
import { LifeboyComponent } from './lifeboy/lifeboy.component';
import { AdaptComponent } from './adapt/adapt.component';
import { AriseComponent } from './arise/arise.component';
import { AchiveComponent } from './achive/achive.component';
import { chat } from "src/app/core-components/exhibition-hall/chat.js";
import { CometChat } from "@cometchat-pro/chat";
import { Exhibition2Component } from './exhibition2/exhibition2.component';
import { Exhibiton3Component } from './exhibiton3/exhibiton3.component';
import { PremiumComponent } from './premium/premium.component';
import { CountryComponent } from './country/country.component';
import { StateComponent } from './state/state.component';
import { MainExhibitorsComponent } from './main-exhibitors/main-exhibitors.component';
import { GermanyComponent } from './germany/germany.component';
import { UkComponent } from './uk/uk.component';
import { DenmarkComponent } from './denmark/denmark.component';
import { AustraliaComponent } from './australia/australia.component';
import { FranceComponent } from './france/france.component';
import { HimachalComponent } from './himachal/himachal.component';
import { RajshtanComponent } from './rajshtan/rajshtan.component';
import { TamilnaduComponent } from './tamilnadu/tamilnadu.component';
import { MadhyapradeshComponent } from './madhyapradesh/madhyapradesh.component';
import { GujratComponent } from './gujrat/gujrat.component';
import { EuropeComponent } from './europe/europe.component';
import { WindeuropeComponent } from './windeurope/windeurope.component';
import { SolareuropeComponent } from './solareurope/solareurope.component';
import { AvereeuropeComponent } from './avereeurope/avereeurope.component';

const routes: Routes = [
  {path:'', component:ExhibitionHallComponent,
  children:[
    {path:'', redirectTo:'mainView' },
    {path:'life', component:ExhibitLifeComponent},
    {path:'lifeboy/:exhibitName', component:LifeboyComponent},
    {path:'adapt', component:AdaptComponent},
    {path:'arise', component:AriseComponent},
    {path:'achive', component:AchiveComponent},
    {path:'exhibition2', component:Exhibition2Component},
    {path:'exhibition3', component:Exhibiton3Component},
    {path:'premium/:exhibitName', component:PremiumComponent},
    {path:'country/:exhibitName', component:CountryComponent},
    {path:'state/:exhibitName', component:StateComponent},
    {path:'mainView', component: MainExhibitorsComponent},
    {path:'germany', component:GermanyComponent},
    {path:'uk', component:UkComponent},
    {path:'denmark', component:DenmarkComponent},
    {path:'australia', component:AustraliaComponent},
    {path:'france', component:FranceComponent},
    {path:'himachal', component:HimachalComponent},
    {path:'rajsthan', component:RajshtanComponent},
    {path:'tamlinadu', component:TamilnaduComponent},
    {path:'madhyapradesh', component:MadhyapradeshComponent},
    {path:'gujrat', component:GujratComponent},
    {path:'EUmain', component:EuropeComponent},
    {path:'EUwind', component:WindeuropeComponent},
    {path:'EUsolar', component: SolareuropeComponent},
    {path:'EUavere', component:AvereeuropeComponent}

  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionHallRoutingModule { }
