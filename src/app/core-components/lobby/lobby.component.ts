import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
declare var introJs: any;
declare var $: any;
import {ToastrService} from 'ngx-toastr';

import * as Clappr from 'clappr';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { ChatService } from 'src/app/services/chat.service';
import { sharedContent } from './shared';
declare var twttr: any;
declare var FB: any;

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})

export class LobbyComponent implements OnInit, AfterViewInit {
  click: any;
  click1: any;
  click2: any;
  click3: any;
  click4: any;
  videoEnd = false;
  receptionEnd = false;
  showVideo = false;
  auditoriumLeft = false;
  auditoriumRight = false;
  exhibitionHall = false;
  registrationDesk = false;
  networkingLounge = false;
  capturePhoto = false
  intro: any;
  player: any;
  actives: any = [];
  liveMsg = false;
  videoSOurce;
  redirect;
  sendername;
  sharedContent=sharedContent;
  slideContent=[];
  innerWidth:any;
  // videoUrl;
  @ViewChild('recepVideo', { static: true }) recepVideo: ElementRef;
  abc: any;
  xyz: any;
  videoplayer='https://d17uqpjc0q0ra5.cloudfront.net/abr/smil:session1.smil/playlist.m3u8';

  constructor(private toastr:ToastrService,private router: Router, private _fd: FetchDataService, private chat: ChatService) { }
  // videoPlayer = 'https://s3.ap-south-1.amazonaws.com/acma.multitvsolution.in/assets/acme/acma-home_1.mp4';
  ngOnInit(): void {
    this.innerWidth=window.innerWidth;
    twttr.widgets.load();
    (function (d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    // this.getUserguide();
    this.slideContent=this.sharedContent;
    this.sharedContent.forEach((ele:any)=>{
      this.slideContent.push({img:ele.img,wd:ele.wd});
    });
    this.stepUpAnalytics('click_lobby');
    // this.audiActive();
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    this.chat.getconnect('toujeo-60');
    this.chat.getMessages().subscribe((data=>{
        console.log('data',data);
      let notify = data.split('_');
      if(notify[0]=='one2one'&& notify[1]==virtual.id){
        this.sendername= notify[2];
        this.liveMsg = true;
        setTimeout(() => {
          this.liveMsg = false;
        }, 10000);
        //this.toastr.success(names+'sent you a new msg');
      }
      }))
    let playVideo: any = document.getElementById('playVideo');
    window.onclick = (event) => {
      if (event.target == playVideo) {
        playVideo.style.display = "none";
        let pauseVideo: any = document.getElementById("video");
        pauseVideo.currentTime = 0;
        pauseVideo.pause();
      }
    }
    this.checkClick();
  }

  ngAfterViewInit() {
    this.playAudio();
  }
  openChat(){
    $('.chatsModal').modal('show');
  }
  closePopup() {
    $('.yvideo').modal('hide');
  }
  // audiActive() {
  //   this._fd.activeAudi().subscribe(res => {
  //     this.actives = res.result;
  //   })
  // }

  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerWidth <= 767) {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });

    } else {
      this.player.resize({
        width: window.innerWidth / 6.69, height: window.innerHeight / 7.95
      });
    }
  }
  playAudio() {
    this.xyz = localStorage.getItem('play');

    if (this.xyz=='stop') {
    this.abc.pause();
      
    } else {
      localStorage.setItem('play', 'stop');
     this.abc= document.getElementById('myAudio');
    this.abc.play();
    
    // this.stopAudio();
    }
    
  }
  stopAudio(){
     localStorage.setItem('play', 'stop');
     this.xyz = localStorage.getItem('play');
    //  console.log(this.xyz)
    //  alert(this.xyz)
  }
  playevent() {
    this.videoEnd = true;
  }
  playReception() {
    // this.router.navigate(['/welcome']);
    this.receptionEnd = true;
    this.showVideo = true;
    let vid: any = document.getElementById("recepVideo");
    vid.play();

  }
  receptionEndVideo() {
    this.router.navigate(['/stadium']);

  }
  gotoAuditoriumFront() {

    this.registrationDesk = true;
    this.showVideo = true;
    let vid: any = document.getElementById("regDeskvideo");
    vid.play();

  }
  playAudiThreeFourEndVideo() {
    this.router.navigate(['/library']);

  }

  // gotoAuditoriumFront() {
  //   this.router.navigate(['/auditorium/front-desk']);
  // }

  playAuditoriumLeft() {
    // if (this.actives[3].status == true) {
      this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
    // }
    // else {
    //   $('.audiModal').modal('show');
    // }

  }
  gotoConferenceHall() {
    this.auditoriumLeft = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audLeftvideo");
      vid.play();
    this.router.navigate(['/auditorium/one']);
  }
  gotoExhibition(name) {
    if (name === 'one') {
      this.router.navigate(['/exhibitionHall']);
    }
    if (name === 'two') {
      this.router.navigate(['/exhibitionHall/exhibition2']);
    }
    if (name === 'three') {
      this.router.navigate(['/exhibitionHall/exhibition3']);
    }
  }
  gotoAuditoriumLeftOnVideoEnd() {
    this.router.navigate(['/gratitude-wall']);
  }
  playAuditoriumRight() {
    // if (this.actives[2].status == true) {
      this.auditoriumRight = true;
      this.showVideo = true;
      let vid: any = document.getElementById("audRightvideo");
      vid.play();
    // }
    // else {
    //   $('.audiModal').modal('show');
    // }
  }
  gotoAuditoriumRightOnVideoEnd() {
    this.router.navigate(['/auditorium/two']);
  }
  playExhibitionHall() {
    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
  }
  gotoExhibitionHallOnVideoEnd() {
    this.router.navigate(['/funzone']);
  }
  playRegistrationDesk() {
    // this.router.navigate(['funzone']);

    this.exhibitionHall = true;
    this.showVideo = true;
    let vid: any = document.getElementById("exhibitvideo");
    vid.play();
  }
  gotoRegistrationDeskOnVideoEnd() {
    this.router.navigate(['/auditorium/front-desk']);
  }
  playNetworkingLounge() {
    this.networkingLounge = true;
    this.showVideo = true;
    let vid: any = document.getElementById("netLoungevideo");
    vid.play();
  }

  gotoNetworkingLoungeOnVideoEnd() {
    this.router.navigate(['/auditorium/one']);
  }
  playCapture(){
    this.capturePhoto = true;
    this.showVideo = true;
    let vid: any = document.getElementById("booth");
    vid.play();
  }
  gotoCaptureVideoOnVideoEnd() {
    this.router.navigate(['/photobooth']);
  }
  playShowVideo(src) {
    this.videoSOurce = src;
    let playVideo: any = document.getElementById("video");
    playVideo.play();
    $('#playVideo').modal('show');
  }

  closeModalVideo() {
    let pauseVideo: any = document.getElementById("video");
    pauseVideo.currentTime = 0;
    pauseVideo.pause();
    $('#playVideo').modal('hide');
  }

  lightbox_open() {
    //this.videoUrl = video;
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    window.scrollTo(0, 0);
    document.getElementById('light').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
    lightBoxVideo.play();

  }
  lightbox_close() {
    let lightBoxVideo: any = document.getElementById('VisaChipCardVideo');
    document.getElementById('light').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
    lightBoxVideo.pause();
  }
  stepUpAnalytics(action) {
    let virtual: any = JSON.parse(localStorage.getItem('virtual'));
    let yyyy: any = new Date().getFullYear();
    let dd: any = new Date().getDate();
    let mm: any = new Date().getMonth() + 1;
    let time: any = new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    const formData = new FormData();
    formData.append('event_id', virtual.event_id);
    formData.append('user_id', virtual.id);
    formData.append('name', virtual.name);
    formData.append('email', virtual.email);
    formData.append('company', virtual.company);
    formData.append('designation', virtual.designation);
    formData.append('action', action);
    formData.append('created', yyyy + '-' + mm + '-' + dd + ' ' + time);
    this._fd.analyticsPost(formData).subscribe(res => {
      console.log('asdf', res);
    });
  }
  getUserguide() {
    if (localStorage.getItem('user_guide') === 'start') {

      this.intro = introJs().setOptions({
        hidePrev: true,
        hideNext: true,
        exitOnOverlayClick: false,
        exitOnEsc: false,
        steps: [
          {
            element: document.querySelector("#reception_pulse"),
            intro: "<div style='text-align:center'>For any queries Relating to the event please visit the Help desk.</div>"
          },
          // {
          //   element: document.querySelectorAll("#networking_pulse")[0],
          //   intro: "<div style='text-align:center'>Click a selfie, visit the wall of fame and Network at the Lounge area.</div>"
          // },
          {
            element: document.querySelectorAll("#exhibition_pulse")[0],
            intro: "<div style='text-align:center'>Do visit the stalls placed in the Exhibition halls.</div>"
          },
          {
            element: document.querySelectorAll("#exhibition_ones")[0],
            intro: "<div style='text-align:center'>You may click on exhibitor logo to visit their stall.</div>"
          },
          {
            element: document.querySelectorAll("#audLeft_pulse")[0],
            intro: "<div style='text-align:center'>Click to attend the live conference on 15th oct 11:00 am to 12:00pm.</div>"
          },
          // {
          //   element: document.querySelectorAll("#audRight_pulse")[0],
          //   intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
          // },
          // {
          //   element: document.querySelector("#frontaudi_pulse"),
          //   intro: "<div style='text-align:center'>All the LIVE sessions will be running in the auditorium on schedule.</div>"
          // },
          // {
          //   element: document.querySelectorAll("#heighlight_pulse")[0],
          //   intro: "<div style='text-align:center'>The agendas for the events running from 21st to 25th can be viewed in agenda section.</div>"
          // },
          // {
          //   element: document.querySelectorAll("#agenda_pulse")[0],
          //   intro: "<div style='text-align:center'>Please leave your valuable feedback on 25th before you leave.</div>"
          // },


        ]
      }).oncomplete(() => document.cookie = "intro-complete=true");

      let start = () => this.intro.start();
      start();
      // if (document.cookie.split(";").indexOf("intro-complete=true") < 0)
      //   window.setTimeout(start, 1000);
    } else {
      return;
    }
  }
  // ngOnDestroy() {
  //   if (localStorage.getItem('user_guide') === 'start') {
  //     let stop = () => this.intro.exit();
  //     stop();
  //   }
  //   localStorage.removeItem('user_guide');
  // }

  gotoExhibtion(id) {
    this.router.navigate(['/exhibitionHall/lifeboy', id]);
  }
  goAudiPoints(){
    let data3 = localStorage.getItem('audipoints');
    if (data3 == '10') {
      
    } else {
      localStorage.setItem('audipoints', '10');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('audipoints')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','auditorium')

      this._fd.clickPoint(formData).subscribe((res: any) => {
  
      });
      this.toastr.success( '10 Points Added');
      
    }
  }
  goGWPoints(){
    let data3 = localStorage.getItem('gwpoints');
    if (data3 == '30') {
      
    } else {
      localStorage.setItem('gwpoints', '30');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('gwpoints')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','gratitudewall')

      this._fd.clickPoint(formData).subscribe((res: any) => {
  
      });
      this.toastr.success( '30 Points Added');

    }
  }
  goPhotoPoints(){
    let data3 = localStorage.getItem('photopoints');
    if (data3 == '30') {
      
    } else {
      localStorage.setItem('photopoints', '30');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('photopoints')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','photobooth')

      this._fd.clickPoint(formData).subscribe((res: any) => {
  
      });
      this.toastr.success( '30 Points Added');

    }
  }
  goLibraryPoints(){
    let data3 = localStorage.getItem('librarypoints');
    if (data3 == '10') {
      
    } else {
      localStorage.setItem('librarypoints', '10');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('librarypoints')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','library')

      this._fd.clickPoint(formData).subscribe((res: any) => {
  
      });
      this.toastr.success( '10 Points Added');

    }
  }
  goQuadranglePoints(){
    let data3 = localStorage.getItem('quadrangle');
    if (data3 == '10') {
      
    } else {
      localStorage.setItem('quadrangle', '10');
      let data = JSON.parse(localStorage.getItem('virtual'));
      let data2 = localStorage.getItem('quadrangle')
      let formData = new FormData();
  
      formData.append('user_id', data.id);
      formData.append('house', data.category);
      formData.append('points',data2)
      formData.append('module','quadrangle')
      this._fd.clickPoint(formData).subscribe((res: any) => {
        
      });
      this.toastr.success( '10 Points Added');

    }
  }
  checkClick(){
    this._fd.getClickData().subscribe((res : any) => {
       this.click = res.result;
       console.log(this.click+'hello')
      //this.click1 = this.click[0].points //green
      //this.click2 = this.click[1].points //yellow
      //this.click3 = this.click[2].points //red 
      //this.click4 = this.click[3].points //blue
    });
  }


}
