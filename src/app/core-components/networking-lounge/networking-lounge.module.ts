import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NetworkingLoungeRoutingModule } from './networking-lounge-routing.module';
import {NetworkingLoungeComponent} from './networking-lounge.component';
import { VgCoreModule } from 'videogular2/compiled/core';
import { VgControlsModule } from 'videogular2/compiled/controls';
import { VgOverlayPlayModule } from 'videogular2/compiled/overlay-play';
import { VgBufferingModule} from 'videogular2/compiled/buffering';
import { VgStreamingModule } from 'videogular2/compiled/streaming';



@NgModule({
  declarations: [NetworkingLoungeComponent],
  imports: [
    CommonModule,
    NetworkingLoungeRoutingModule,
    VgCoreModule,
    VgControlsModule,
    VgBufferingModule,
    VgOverlayPlayModule,
    VgStreamingModule
  ]
})
export class NetworkingLoungeModule { }
