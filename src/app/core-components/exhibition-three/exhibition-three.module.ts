import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionThreeRoutingModule } from './exhibition-three-routing.module';
import { ExhibtionThreeComponent } from './exhibtion-three/exhibtion-three.component';


@NgModule({
  declarations: [ExhibtionThreeComponent],
  imports: [
    CommonModule,
    ExhibitionThreeRoutingModule
  ]
})
export class ExhibitionThreeModule { }
