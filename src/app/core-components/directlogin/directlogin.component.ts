import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-directlogin',
  templateUrl: './directlogin.component.html',
  styleUrls: ['./directlogin.component.scss']
})
export class DirectloginComponent implements OnInit {
user;
  constructor(private route:ActivatedRoute,private auth: AuthService,private router:Router) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.router.navigate(['/login']);
    }, 3000);
  }

}

