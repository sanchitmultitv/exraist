import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ExhibitorloginRoutingModule } from './exhibitorlogin-routing.module';
import { ExhibitorloginComponent } from './exhibitorlogin.component';
import { ExhibitordashboardComponent } from './exhibitordashboard/exhibitordashboard.component';


@NgModule({
  declarations: [ExhibitorloginComponent, ExhibitordashboardComponent],
  imports: [
    CommonModule,
    ExhibitorloginRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ExhibitorloginModule { }
