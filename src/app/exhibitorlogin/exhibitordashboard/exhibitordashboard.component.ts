import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import * as XLSX from 'xlsx'; 
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ChatService } from 'src/app/services/chat.service';
declare var $: any;

@Component({
  selector: 'app-exhibitordashboard',
  templateUrl: './exhibitordashboard.component.html',
  styleUrls: ['./exhibitordashboard.component.scss']
})
export class ExhibitordashboardComponent implements OnInit {
  textMessage = new FormControl('');
  msg;
  qaList;
  interval;
  exhibiton: any = [];
  // documents:any=[];
  video: any;
  // documents: any;
  loop: any = [];
  exhibitionName: any;
  brochures: any;
  banner: any;
  salesPerson: any;
  salesGuy: any;
  documents: any;
  videos: any;
  carddata: any;
  documented: any;
  localItem: any;
  localId: any;
  cards: any;
  calls: any;
  products: any;
  exhibit: any = 24;
  chatPerson: any;
  chatGuy: any;
  getAnal: any;
  fileToUpload: File = null;
  fileToUploadTwo: File = null;
  ques: any;
  guy_id: any;
  last_id: any;
  chatGuy2:any;
  exID:any;
  // guy_id2:any;
  postdoc = new FormGroup({
    title: new FormControl(''),
    file: new FormControl(''),
  });

  postreply = new FormGroup({
    answer: new FormControl('')
  })

  // delVideo = new FormGroup({
  //   id: new FormControl(''),
  //   email: new FormControl(''),
  //   time: new FormControl(''),
  //   name: new FormControl('')

  // })

  postBrochure = new FormGroup({
    brochure_name: new FormControl(''),
    brochure_file: new FormControl(''),
  });

  constructor(public router: Router, private auth: AuthService, private sanitiser: DomSanitizer, private _fd: FetchDataService, private route: ActivatedRoute, private formBuilder: FormBuilder, private httpClient: HttpClient, private chat: ChatService) { }
  // @ViewChild('iframe') iframe: ElementRef;

  fileName= 'Visitor_Stalls.xlsx';  
  fileName2= 'Card_drop.xlsx';  
  fileName3= 'Live_Chat.xlsx';  
  fileName4= 'Chat.xlxs';


  exportexcel(): void 
      {
         /* table id is passed over here */   
         let element = document.getElementById('excel-visitor'); 
         const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
  
         /* generate workbook and add the worksheet */
         const wb: XLSX.WorkBook = XLSX.utils.book_new();
         XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
         /* save to file */
         XLSX.writeFile(wb, this.fileName);
        
      }
      exportexcel2(): void 
      {
         /* table id is passed over here */   
         let element = document.getElementById('excel-card'); 
         const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
  
         /* generate workbook and add the worksheet */
         const wb: XLSX.WorkBook = XLSX.utils.book_new();
         XLSX.utils.book_append_sheet(wb, ws, 'Sheet2');
  
         /* save to file */
         XLSX.writeFile(wb, this.fileName2);
        
      }
      exportexcel3(): void 
      {
         /* table id is passed over here */   
         let element = document.getElementById('excel-chat'); 
         const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
  
         /* generate workbook and add the worksheet */
         const wb: XLSX.WorkBook = XLSX.utils.book_new();
         XLSX.utils.book_append_sheet(wb, ws, 'Sheet3');
  
         /* save to file */
         XLSX.writeFile(wb, this.fileName3);
        
      }
      exportexcel4(): void 
      {
         /* table id is passed over here */   
         let element = document.getElementById('chat-excel'); 
         const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
  
         /* generate workbook and add the worksheet */
         const wb: XLSX.WorkBook = XLSX.utils.book_new();
         XLSX.utils.book_append_sheet(wb, ws, 'Sheet4');
  
         /* save to file */
         XLSX.writeFile(wb, this.fileName3);
        
      }
  ngOnInit(): void {
   
      // this.getChatGuy();
     setInterval(() => {
        this.getChatGuy(); 
      }, 60000);
   
    this.getChatGuy();
    var helli = JSON.parse(localStorage.getItem('exhibitlogin')).exhibition_id;
    this.exID =  helli;
    // alert(this.exID)
    //  this.getUserId();
    this.chat.getconnect('toujeo-54');
    this.chat.getMessages().subscribe((data => {
      console.log('socketdata', data);
      if (data == 'question_post') {
        var guy_id2 = JSON.parse(localStorage.getItem('exhibitlogin'));
        console.log(guy_id2);
        // alert(this.guy_id2)
        var guy2 = guy_id2.exhibition_id;
        //  debugger
        this._fd.getParticularChat(guy2, this.last_id).subscribe((res: any) => {
          //  alert('res')
          this.chatGuy = res.result;
          console.log(this.chatGuy);
          // alert(this.chatGuy);
        });
        setTimeout(() => {
         this.getChatGuy();
        }, 1000);
      //  setTimeout(() => {
      //   this.getChatGuy();
      //  }, 1000);
      }
    }));
    //  this.deleteDoc();getChatGuy
    this.analyticsGet();
    this.getChatGuy();
    // this.getExhibitData();
    this.getCardDrop();
    this.getScheduleCall();
    // this.chatget();
    // var hello = localStorage.getItem('exhibition_id');
    // var hello2 = localStorage.getItem('Expassword');
    // alert(hello)
    // alert(hello2)
    const menuIconEl = $('.menu-icon');
    const sidenavEl = $('.sidenav');
    const sidenavCloseEl = $('.sidenav__close-icon');

    // Add and remove provided class names
    function toggleClassName(el, className) {
      if (el.hasClass(className)) {
        el.removeClass(className);
      } else {
        el.addClass(className);
      }
    }

    // Open the side nav on click
    menuIconEl.on('click', function () {
      toggleClassName(sidenavEl, 'active');
    });

    // Close the side nav on click
    sidenavCloseEl.on('click', function () {
      toggleClassName(sidenavEl, 'active');
    });

    // this.docUpload();
    // alert(localStorage.getItem('exemail'))

  }
  closePopup() {
    $('.chat-v').modal('hide');
  }
  // getQA(){
  //   //  console.log('exhibitonid',this.exhibition_id);
  //     let data = JSON.parse(localStorage.getItem('exhibitlogin'));
  //    // console.log('uid',data.id);
  //     this._fd.getanswers(data.id,this.exhibition_id).subscribe((res=>{
  //       //console.log(res);
  //       this.qaList = res.result;
  //       // alert('hello');
  //     }))

  //   }
  //   postQuestion(value){
  //     let data = JSON.parse(localStorage.getItem('exhibitlogin'));
  //   //  console.log(value, data.id);
  //   // this.getQA();
  //     this._fd.askQuestions(data.id,value,this.exhibition_id).subscribe((res=>{
  //       //console.log(res);
  //       // this.getQA();
  //       if(res.code == 1){
  //         this.msg = 'Submitted Succesfully';
  //       // var d = $('.chat_message');
  //       // d.scrollTop(d.prop("scrollHeight"))
  //       }
  //       this.getQA();

  //       setTimeout(() => {
  //         $('#chat_messaged')[0].scrollTop = $('#chat_messaged')[0].scrollHeight-100;
  //         this.msg = '';
  // //$('.liveQuestionModal').modal('hide');
  //       }, 2000);
  //       // setTimeout(() => {
  //       //   this.msg = '';
  //       //   $('.liveQuestionModal').modal('hide');
  //       // }, 2000);
  //       this.textMessage.reset();
  //     }))
  //   }

  // down(){
  //   var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  //       return base.File(fileBytes, contentType, "Download.xlsx");
  // }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

  // deleteDoc(){
  //   var doc_id = JSON.parse(localStorage.getItem('exhibitlogin'));

  //     alert(doc_id);
  //     var deleteId = doc_id.id;
  //     var deleteExhi = doc_id.exhibition_id;
  //     console.log(doc_id.exhibition_id)
  //     console.log(doc_id.id)

  //   this._fd.delDoc(deleteId, deleteExhi).subscribe((res => {

  //     if (res.code === 1) {
  //       alert('Successfully Submitted');
  //       this.postBrochure.reset();

  //     } else {
  //       alert('Invalid Data');
  //       this.postBrochure.reset();
  //     }
  //     // localStorage.setItem('exhibitor','dashboard');

  //   }));
  // }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  getChatGuy() {
    this.guy_id = JSON.parse(localStorage.getItem('exhibitlogin'));
    var guy = this.guy_id.exhibition_id
    // console.log(guy.exhibition_id);
    
    this._fd.getChatPerson(guy).subscribe((res: any) => {
      // alert(res)
      this.chatPerson = res.result;
      console.log(this.chatPerson);
    });
  }

  

  chatget(id) {
    console.log(id);
    this.last_id = id
    // var use_id = id
    // console.log()
    // console.log(id.cards[0].id);
    // alert(id)
    var guy_id2 = JSON.parse(localStorage.getItem('exhibitlogin'));
    console.log(guy_id2);
    // alert(this.guy_id2)
    var guy2 = guy_id2.exhibition_id;
    //  debugger
    this._fd.getParticularChat(guy2, this.last_id).subscribe((res: any) => {
      //  alert('res')
      this.chatGuy = res.result;
      console.log(this.chatGuy);
      // alert(this.chatGuy);
    });
    setInterval(function(){ 
      this._fd.getParticularChat(guy2, this.last_id).subscribe((res: any) => {
        //  alert('res')
        this.chatGuy = res.result;
        console.log(this.chatGuy);
        // alert(this.chatGuy);
      });
    }, 3000);
  }

  // getUserId() {
    
  //   var guy_id2 = JSON.parse(localStorage.getItem('exhibitlogin'));
  //   console.log(guy_id2);
    
  //   this._fd.getRed().subscribe((res: any) => {
      
  //     this.chatGuy2 = res.result;
  //     console.log(this.chatGuy2);
      
  //   });
  // }

  showhide(question_id) {
    // console.log(question_id);
    this.ques = question_id
    // console.log(this.ques);
    $('#question_id').val(question_id);
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  docpost() {
    var hello = localStorage.getItem('exhibition_id');
    // alert(hello)

    let formData = new FormData();
    // alert(formData)
    formData.append('doc', this.fileToUpload);
    formData.append('title', this.postdoc.value.title);
    formData.append('exhibition_id', hello);

    // let user = {
    //   titles: this.postdoc.value.title,
    //   exhibition_id : 24,
    //    files : this.fileToUpload
    // };

    // console.log(user);
    // alert(user.files)
    // alert(user.titles)
    // console.log("password",user.password)
    //  alert("hi")
    // alert(this.user);
    // console.log(this.fileToUpload);
    this._fd.postDoc(formData).subscribe((res => {

      if (res.code === 1) {
        alert('Successfully Submitted');
        this.postdoc.reset();

      } else {
        alert('Invalid Data');
        this.postdoc.reset();
      }
      // localStorage.setItem('exhibitor','dashboard');

    }));
  }

  // brochure 
  handleBrochureInput(files: FileList) {
    this.fileToUploadTwo = files.item(0);
  }

  brochurepost() {

    var hello2 = localStorage.getItem('exhibition_id');
    let formDataTwo = new FormData();
    formDataTwo.append('doc', this.fileToUploadTwo);
    formDataTwo.append('title', this.postBrochure.value.brochure_name);
    formDataTwo.append('exhibition_id', hello2);

    // let user = {
    //   titles: this.postBrochure.value.title,
    //   exhibition_id : 24,
    //    files : this.fileToUpload
    // };

    // console.log(user);
    // alert(user.files)
    // alert(user.titles)
    // console.log("password",user.password)
    //  alert("hi")
    // alert(this.user);
    // console.log(this.fileToUpload);
    this._fd.postBrochure(formDataTwo).subscribe((res => {

      if (res.code === 1) {
        // alert('Successfully Submitted');
        this.postBrochure.reset();
        window.location.reload();

      } else {
        // alert('Invalid Data');
        this.postBrochure.reset();
      }
      // localStorage.setItem('exhibitor','dashboard');

    }));
  }

  docreply() {
    // var hello = JSON.parse(localStorage.getItem('exhibitlogin'));
    // alert(hello)
    // alert(this.ques);
    // var loop = '411'
    console.log(this.postreply.value);
    let formData = new FormData();
    // alert(formData)
    // formData.append('doc', this.fileToUpload);
    formData.append('answer', this.postreply.value.answer);
    formData.append('question_id', this.ques);

    this._fd.postReply(formData).subscribe((res => {

      if (res.code === 1) {
        // alert('Successfully Submitted');
        this.postreply.reset();
        document.getElementById("myDIV").style.display = "none";
        // $("#myDIV").trigger("click");

        var guy_id2 = JSON.parse(localStorage.getItem('exhibitlogin'));
        console.log(guy_id2);
        // alert(this.guy_id2)
        var guy2 = guy_id2.exhibition_id;
        //  debugger
        this._fd.getParticularChat(guy2, this.last_id).subscribe((res: any) => {
          //  alert('res')
          $("#myDIV").trigger("click");
          $("#myDIV").trigger("toggle");
          this.chatGuy = res.result;
          console.log(this.chatGuy);
          // alert(this.chatGuy);
        });
        // $("#chatpersonal").trigger("click");
      } else {
        // alert('Invalid Data');
        this.postreply.reset();
        $("#chatpersonal").trigger("click");

      }
      // localStorage.setItem('exhibitor','dashboard');

    }));

  }
  delVideo(ec_id,email,time) {
    this.localId = JSON.parse(localStorage.getItem('exhibitlogin')).name;
    // alert(id);
    // alert(email);
    // alert(time);

    let del = {
        idd: ec_id,
        emaill : email,
        timee : time,
        exhii: this.localId
      };

    // alert(hello)
    // var locid = this.localId.exhibition_id
    // alert(this.ques);
    // var loop = '411'
    


    // alert(ec_id);
    // console.log(this.postreply.value);
    // let formData = new FormData();
    // alert(formData)
    // formData.append('doc', this.fileToUpload);
    // formData.append('id',this.postreply.value.id);
    // formData.append('email',email);
    // formData.append('email',email)

    this._fd.delCall(del).subscribe((res => {
      if (res.code === 1) {
        // alert('Successfully Submitted');
        // this.postreply.reset();
        // document.getElementById("myDIV").style.display = "none";
        // $("#myDIV").trigger("click");

        // var guy_id2 = JSON.parse(localStorage.getItem('exhibitlogin'));
        // console.log(guy_id2);
        // var guy2 = guy_id2.exhibition_id;
        // this._fd.getParticularChat(guy2, this.last_id).subscribe((res: any) => {
          // $("#myDIV").trigger("click");
          // $("#myDIV").trigger("toggle");
        //   this.chatGuy = res.result;
        //   console.log(this.chatGuy);
        // });
      } else {
        alert('Invalid Data');
        // this.postreply.reset();
        // $("#chatpersonal").trigger("click");

      }
    }));

  }
  closereply() {
    $('.chatts').modal('hide');
  }



  getExhibitData() {
    this.localItem = localStorage.getItem('exhibition_id');
    var hell = this.localItem
    // console.log(hell);
    this._fd.getExhibitionTwo(hell).subscribe(res => {
      console.log('exhibition', res);
      this.brochures = res.result[0].brochure;
      this.documents = res.result[0].document;
      this.products = res.result[0].product;
      this.salesPerson = res.result[0];
      this.salesGuy = res.result[0].sales;
      this.videos = res.result;

      // console.log(this.videos);
      // this.banner = this.exhibiton[0].banner;
      // this.salesPerson = res.result[0].sales;
      // this.s = res.result[0].product[0].url;
    });
  }
  getCardDrop() {
    this.localItem = localStorage.getItem('exhibition_id');
    var hells = this.localItem
    // console.log(hells);
    this._fd.getCard(hells).subscribe((res: any) => {
      // console.log('carddata', res);
      // this.carddata = res.result;

      this.cards = res.result;
      //  var carddata =  this.cards
      //   console.log(this.cards);
    });
  }

  getScheduleCall() {
    this.localId = JSON.parse(localStorage.getItem('exhibitlogin'));
    var locid = this.localId.exhibition_id
    console.log(locid);
    // alert(locid)
    this._fd.scheduleCallGet(locid).subscribe((res: any) => {
      //alert('locid');
      console.log('calldata', locid);
      console.log('hell', res.result);

      this.calls = res.result;

    })
  }

  logoutexhibitor() {
    let user_id = JSON.parse(localStorage.getItem('exhibitlogin')).id;
    this.auth.logout(user_id).subscribe(res => {
      this.router.navigate(['/exhibitorlogin']);
      localStorage.clear();
    });
  }

  changestalls(value) {
    // alert(value);
    //let exhi_id = JSON.parse(localStorage.getItem('exhibitlogin')).exhibition_id;
    // alert(exhi_id);
    
    this._fd.getAnalytics(value).subscribe((res: any) => {
      
      // console.log("sanchit",res.result.created);
      this.getAnal = res.result;
          console.log("sanchit",this.getAnal);
    });

  }

  // DownExcel(xl){
  //   alert('Hmm')
  //   this._fd.downXL(xl).subscribe((res: any) => {
  //     alert('Hell');
  //         console.log("sanchit",this.getAnal);
  //   });
  // }

  analyticsGet(){
    let exhi_id = JSON.parse(localStorage.getItem('exhibitlogin')).exhibition_id;
    // alert(exhi_id);
    
    this._fd.getAnalytics(exhi_id).subscribe((res: any) => {
      
      // console.log("sanchit",res.result.created);
      this.getAnal = res.result;
          console.log("sanchit",this.getAnal);
    });
  }
}
