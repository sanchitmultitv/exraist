import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhibitordashboardComponent } from './exhibitordashboard.component';

describe('ExhibitordashboardComponent', () => {
  let component: ExhibitordashboardComponent;
  let fixture: ComponentFixture<ExhibitordashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExhibitordashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExhibitordashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
