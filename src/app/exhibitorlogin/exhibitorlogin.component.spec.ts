import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhibitorloginComponent } from './exhibitorlogin.component';

describe('ExhibitorloginComponent', () => {
  let component: ExhibitorloginComponent;
  let fixture: ComponentFixture<ExhibitorloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExhibitorloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExhibitorloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
