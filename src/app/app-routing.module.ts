import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectloginComponent } from './core-components/directlogin/directlogin.component';
import { AuthGuard } from './shared/auth/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'prefix' },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  { path: 'signup', loadChildren: () => import('./signup/signup.module').then(m => m.SignupModule) },
  { path: '', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule), canActivate:[AuthGuard] },
  { path: 'exhibitorlogin', loadChildren: () => import('./exhibitorlogin/exhibitorlogin.module').then(m => m.ExhibitorloginModule) },
  {path:'directlogin', component:DirectloginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {'useHash': true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
